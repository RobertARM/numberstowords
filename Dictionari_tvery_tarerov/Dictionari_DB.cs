﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dictionari_tvery_tarerov
{
    class Dictionari_DB
    {
        static public void NumToString(int number, int bazmpat)
        {
            var num = new Dictionary<int, string>();
            num.Add(0, "Zro");
            num.Add(1, "Mek");
            num.Add(2, "Erku");
            num.Add(3, "Ereq");
            num.Add(4, "Chors");
            num.Add(5, "Hing");
            num.Add(6, "Vec");
            num.Add(7, "Jot");
            num.Add(8, "Ut");
            num.Add(9, "In@");
            num.Add(10, "tas");
            num.Add(20, "Qsan");
            num.Add(30, "Eresun");
            num.Add(40, "Qarasun");
            num.Add(50, "Hisun");
            num.Add(60, "Vatsun");
            num.Add(70, "Jotanasun");
            num.Add(80, "Utsun");
            num.Add(90, "Inysun");
            num.Add(100, "Haryur");
            num.Add(1000, "Hazar");

            if (bazmpat == 10 && number > 1 && number < 10)
                Console.Write(num[number * bazmpat]);

            else if (bazmpat == 1 && number >= 0)
                Console.Write(num[number]);

            else if (number > 10 && number < 20)
            {
                number %= 10;
                Console.Write(num[10] + "n@" + num[number]);
            }
            else if (bazmpat >= 10 && number == 1)
            {
                Console.Write(num[bazmpat]+" ");
            }
            else if (number == 0)
            {
                Console.Write(num[number]);
            }

            else
                Console.Write(num[number] + num[bazmpat]+" ");

        }
        static public int NumberSplit(int num, out int bazmapatik)
        {
            int num1 = num;
            //int num2 = num;
            double count = 0;
            while (!(num1 == 0))
            {
                num1 /= 10;
                count++;
            }
            double bazmapatik1 = (Math.Pow(10, count - 1));
            bazmapatik = (int)bazmapatik1;
            if (!(bazmapatik == 0))
            {
                int num2 = num / bazmapatik;

                return num2;

            }

            return 0;
        }
    }
}

